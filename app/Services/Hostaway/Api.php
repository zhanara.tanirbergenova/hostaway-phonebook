<?php

namespace App\Services\Hostaway;

use Phalcon\Di;
use GuzzleHttp\Client;

class Api
{
    const BASE_URI        = 'https://api.Hostaway.com/';
    const COUNTRIES_URI   = 'countries';
    const TIMEZONES_URI   = 'timezones';
    const TIMEOUT         = 10;

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * Api constructor.
     */
    public function __construct()
    {
        $this->client =  new Client([
            // Base URI is used with relative requests
            'base_uri' => self::BASE_URI,
            // You can set any number of default request options.
            'timeout'  => self::TIMEOUT
        ]);
    }

    /**
     * Getting country codes
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCountryCodes()
    {
        $cachedCountryCodes = Cache::getCountryCodes();
        if (!empty($cachedCountryCodes)) {
            return $cachedCountryCodes;
        }

        try {
            $countryCodes = $this->getContent(self::COUNTRIES_URI);
            Cache::saveCountryCodes($countryCodes);
            return $countryCodes;
        } catch (\Exception $e) {
            $logMessage = "Error in request getCountryCodes:\n" .
                "Error Code: {$e->getCode()}\n" .
                "Error Message: {$e->getMessage()}\n";

            Di::getDefault()['logger']->error($logMessage);
        }
    }

    /**
     * Getting timezones
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTimezones()
    {
        $cachedTimezones = Cache::getTimezones();
        if(!empty($cachedTimezones)) {
            return $cachedTimezones;
        }

        try {
            $timezones = $this->getContent(self::TIMEZONES_URI);
            Cache::saveTimezones($timezones);
            return $timezones;
        } catch (\Exception $e) {
            $logMessage = "Error in request getTimezones:\n" .
                "Error Code: {$e->getCode()}\n" .
                "Error Message: {$e->getMessage()}\n";

            Di::getDefault()['logger']->error($logMessage);
        }
    }

    /**
     * Getting content using Guzzle Http
     *
     * @param $url
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContent($url)
    {
        $content = $this->client->request('GET', $url)->getBody()->getContents();
        return array_keys((json_decode($content, true))['result']);
    }
}