<?php
/**
 * Created by PhpStorm.
 * User: zhanarok
 * Date: 2019-08-11
 * Time: 16:11
 */

namespace App\Services\Hostaway;

use Phalcon\Di;

class Cache
{
    /**
     * redis key of country codes storage
     */
    const COUNTRY_CODES_KEY = 'country_codes';

    /**
     * redis key of timezones storage
     */
    const TIMEZONES_KEY     = 'timezones';

    /**
     * Saving countryCodes to redis
     *
     * @param array $countryCodes
     */
    public static function saveCountryCodes($countryCodes = [])
    {
        Di::getDefault()['cache']->save(self::COUNTRY_CODES_KEY, $countryCodes);
    }

    /**
     * Getting country codes from redis
     *
     * @return array
     */
    public static function getCountryCodes()
    {
        return Di::getDefault()['cache']->get(self::COUNTRY_CODES_KEY);
    }

    /**
     * Saving timezones to redis
     *
     * @param array $timezones
     */
    public static function saveTimezones($timezones = [])
    {
        Di::getDefault()['cache']->save(self::TIMEZONES_KEY, $timezones);
    }

    /**
     * Getting timezones from redis
     *
     * @return mixed
     */
    public static function getTimezones()
    {
        return Di::getDefault()['cache']->get(self::TIMEZONES_KEY);
    }
}