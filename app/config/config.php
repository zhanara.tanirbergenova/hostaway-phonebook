<?php

use Phalcon\Config;
use Phalcon\Logger;

defined('APP_PATH') || define('APP_PATH', dirname(__DIR__, 1));

return new Config([
    'database' => [
        'adapter'  => 'MySql',
        'host'     => 'localhost',
        'username' => 'admin',
        'password' => 'adminadmin',
        'dbname'   => 'phonebook'
    ],
    'logger' => [
        'format'   => '[%date%][%type%] %message%', // record format
        'fileName' => APP_PATH . '/logs/[%date%].log', // file name
        'level'    => Logger::CUSTOM, // minimum level to log
    ],
    'cache' => [
        'lifetime' => 172800, // 2 days
        'redis' => [
            'prefix' => 'phonebook_', // record prefix
            'host'   => '127.0.0.1',
            'port'   => 6379,
            'index'  => 0 // Database number in Redis. Must be integer
        ],
    ],
    'auth' => [
        'secretKey' => '923753F2317FC1EE5B52DF23951B1',
        'payload' => [
            'exp' => 1440,
            'iss' => 'phalcon-jwt-auth'
        ],
        'ignoreUri' => [
            'regex:/session/'
        ]
    ],
    'Services' => [
        'google' => [
            'clientId'     => '465177371888-gavugf04ksos25d1rcckv1i43bkt2d28.apps.googleusercontent.com',
            'clientSecret' => 'nPTRLJgrRSbCmLbManWXt2Uo'
        ]
    ]
]);