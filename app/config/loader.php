<?php

use Phalcon\Loader;

defined('APP_PATH') || define('APP_PATH', dirname(__DIR__, 1));

$loader = new Loader();

$loader->registerNamespaces([
      'App' => APP_PATH
]);
$loader->register();


require_once BASE_PATH . '/vendor/autoload.php';