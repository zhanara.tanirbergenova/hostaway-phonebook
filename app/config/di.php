<?php

use Phalcon\Cache\Frontend\Data as FrontendData;
use Phalcon\Cache\Backend\Redis;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Logger\Multiple as Logger;
use App\Services\Hostaway\Api;
use App\Library\Oauth;

/**
 * Dependency Injector container
 */
$di = new FactoryDefault();

/**
 * Registering config service
 */
$di->setShared('config', $config);

/**
 * Registering database service
 */
$di->setShared(
    'db',
    function () use ($config) {
        return new DbAdapter([
            'host' => $config->database->host,
            'username' => $config->database->username,
            'password' => $config->database->password,
            'dbname' => $config->database->dbname
        ]);
    }
);

$di->setShared(
    'oauth',
    function () {
        $auth = new Oauth();
        $auth->setDI($this);
        return $auth;
    }
);

/**
 * Registering oauthProviderGoogle service
 */
$di->setShared(
    'oauthProviderGoogle',
    function () use ($config) {
        $configProvider = $config->services->google;
        $relativePath   = '/session/oauth/?provider=google';
        $redirectUri    = "http://{$_SERVER['HTTP_HOST']}{$relativePath}";

        return new \League\OAuth2\Client\Provider\Google([
            'clientId'     => $configProvider->clientId,
            'clientSecret' => $configProvider->clientSecret,
            'redirectUri'  => $redirectUri,
            'accessType'   => 'offline'
        ]);
    }
);

/**
 * Registering Hostaway service for getting country codes and timezones
 */
$di->setShared(
    'Hostaway',
    function () {
        return new Api();
    }
);

/**
 * Logger
 */
$di->set(
    'logger',
    function () use ($config) {
        $logger = new Logger();
        $fileName = strtr($config->logger->fileName, ['[%date%]' => date('Y-m-d')]);

        try {
            $fileAdapter = new FileLogger($fileName);
        } catch (\Exception $e) {
            error_log($e);
        }

        if (isset($fileAdapter)) {
            if (isset($config->logger->level)) {
                $fileAdapter->setLogLevel($config->logger->level);
            }

            if (isset($config->logger->format)) {
                $formatter = new LineFormatter(strtr($config->logger->format, []));
                $fileAdapter->setFormatter($formatter);
            }

            $logger->push($fileAdapter);
        }
        return $logger;
    }
);

$di->set(
    'cache',
    function () use ($config) {
        // Frontend Cache
        $frontCache = new FrontendData(['lifetime' => $config->cache->lifetime]);

        // Connect to redis (Backend Cache)
        $redisClient = new Redis($frontCache, $config->cache->redis);

        return $redisClient;
    },
    true
);

return $di;