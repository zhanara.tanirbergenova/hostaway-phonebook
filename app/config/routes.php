<?php

use \Phalcon\Mvc\Micro\Collection;
use App\Controllers\ContactsController;
use App\Controllers\SessionController;

$contacts = new Collection();
$contacts->setHandler(new ContactsController());
$contacts->setPrefix('/contacts');
$contacts->get('/:params', 'indexAction');
$contacts->get('/search/{name}/:params', 'searchAction');
$contacts->post('/create', 'createAction');
$contacts->get('/read/{id:\d+}', 'readAction');
$contacts->put('/update/{id:\d+}', 'updateAction');
$contacts->delete('/delete/{id:\d+}', 'deleteAction');
$app->mount($contacts);

$userAuth = new Collection();
$userAuth->setHandler(new SessionController());
$userAuth->setPrefix('/session');
$userAuth->get('/oauthRedirect/:params', 'oauthRedirectAction');
$userAuth->get('/oauth/:params', 'oauthAction');
$app->mount($userAuth);

$app->notFound(
    function () use ($app) {
        $app->response->setStatusCode(404, 'Not Found');
        $app->response->sendHeaders();
        $message = 'Nothing to see here. Move along....';
        $app->response->setContent($message);
        $app->response->send();
    }
);




