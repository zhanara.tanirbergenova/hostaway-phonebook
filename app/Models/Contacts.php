<?php

namespace App\Models;

use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\Message;
use \Phalcon\Validation;



class Contacts extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $country_code;

    /**
     *
     * @var string
     */
    public $timezone;

    /**
     *
     * @var string
     */
    public $inserted_on;

    /**
     *
     * @var string
     */
    public $updated_on;

    /**
     *
     * @var string
     */
    public $deleted_on;

    /**
     * @var []
     */
    private $countryCodes;

    /**
     * @var []
     */
    private $timezones;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("phonebook");
        $this->setSource("contacts");
        $this->countryCodes = $this->getDI()['Hostaway']->getCountryCodes();
        $this->timezones    = $this->getDI()['Hostaway']->getTimezones();

        $this->addBehavior(
            new SoftDelete(
                [
                    'field' => 'deleted_on',
                    'value' => date('Y-m-d H:i:s')
                ]
            )
        );
    }

    /**
     * Returning table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contacts';
    }

    /**
     * Setting the creation date
     */
    public function beforeCreate()
    {
        $this->inserted_on = date('Y-m-d H:i:s');
        $this->updated_on = $this->inserted_on;
    }

    /**
     * Setting the modification date
     */
    public function beforeUpdate()
    {
        // Set the modification date
        $this->updated_on = date('Y-m-d H:i:s');
    }

    /**
     * @param $messages
     * @return bool
     */
    public function beforeValidation()
    {

        if (!isset($this->countryCodes)) {
            $message = new Message('Country codes failed to load', 'country_code');
            $this->appendMessage($message);
            return false;
        } elseif (!isset($this->timezones)) {
            $message = new Message('Timezones failed to load', 'timezone');
            $this->appendMessage($message);
            return false;
        }

        return true;
    }

    /**
     * Function that validates fields
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'phone',
            new Validation\Validator\Regex([
                'pattern' => '/^\+\d{14}$/',
                'message' => 'Incorrect phone number format'
            ])
        );
        $validator->add(
            'phone',
            new Validation\Validator\Uniqueness([
                'message' => 'The phone number must be unique'
            ])
        );
        $validator->add(
            'country_code',
            new Validation\Validator\InclusionIn(
                [
                    'message' => 'The country code must be in the Hostaway list',
                    'domain'  => $this->countryCodes,
                ]
            )
        );
        $validator->add(
            'timezone',
            new Validation\Validator\InclusionIn(
                [
                    'message' => 'The timezone must be in the Hostaway list',
                    'domain'  => $this->timezones,
                ]
            )
        );
        return $this->validate($validator);
    }

}
