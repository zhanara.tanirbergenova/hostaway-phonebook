<?php

namespace App\Controllers;
 
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;
use Phalcon\Paginator\Adapter\Model as Paginator;
use App\Models\Contacts;


class ContactsController extends Controller
{
    const DEFAULT_PAGE = 1;

    const PER_PAGE     = 100;

    /**
     * Index action (page and limit - optional params)
     *
     * @return Response
     */
    public function indexAction()
    {
        $contacts = Contacts::find("deleted_on IS NULL");
        $page     = $this->request->getQuery('page') ?? self::DEFAULT_PAGE;
        $limit    = $this->request->getQuery('limit') ?? self::PER_PAGE;
        $limit    = ($limit > self::PER_PAGE) ? self::PER_PAGE : $limit;

        $this->response->setJsonContent($this->getPaginator($contacts, $limit, $page));

        return $this->response;
    }

    /**
     * Searches for contacts
     *
     * @param string $name
     * @return Response
     */
    public function searchAction($name)
    {
        $name     = $this->filter->sanitize($name, 'alphanum');
        $name     = $this->filter->sanitize($name, 'lower');
        $contacts = Contacts::find('deleted_on IS NULL AND (first_name LIKE "%' . $name .
                                  '%" OR last_name LIKE "%' . $name . '%")' );
        $page     = $this->request->getQuery('page') ?? self::DEFAULT_PAGE;
        $limit    = $this->request->getQuery('limit') ?? self::PER_PAGE;
        $limit    = ($limit > self::PER_PAGE) ? self::PER_PAGE : $limit;

        $this->response->setJsonContent($this->getPaginator($contacts, $limit, $page));

        return $this->response;

    }

    /**
     * Creating a new contact
     *
     * @return Response
     */
    public function createAction()
    {
        $params = $this->request->getJsonRawBody(true);

        if (!$params) {
            $this->response->setStatusCode(400);
            $this->response->setJsonContent(['input_error' =>'Incorrect JSON format']);
        } else {
            if (isset($params['phone'])) {
                $params['phone'] = str_replace(' ', '', $params['phone']);
            }
            $contact = new Contacts($params);
            if (!$contact->save()) {
                $errors = [];
                $messages = $contact->getMessages();
                foreach ($messages as $message) {
                    $errors[$message->getField()] = $message->getMessage();
                }
                $this->response->setStatusCode(400);
                $this->response->setJsonContent($errors);
            } else {
                $this->response->setJsonContent($contact->toArray());
            }
        }

        return $this->response;
    }

    /**
     * Updating a contact
     *
     * @param string $id
     * @return Response
     */
    public function updateAction($id)
    {
        $contact = Contacts::findFirst("id = " . $id . " AND deleted_on IS NULL");
        $params = $this->request->getJsonRawBody(true);

        if(!$contact) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['input_error' =>'Contact not found']);
        } elseif (!$params) {
            $this->response->setStatusCode(400);
            $this->response->setJsonContent(['input_error' =>'Incorrect JSON format']);
        } else {
            if (!$contact->update($params)) {
                $errors = [];
                $messages = $contact->getMessages();
                foreach ($messages as $message) {
                    $errors[$message->getField()] = $message->getMessage();
                }
                $this->response->setStatusCode(400);
                $this->response->setJsonContent($errors);
            } else {
                $this->response->setJsonContent($contact->toArray());
            }
        }

        return $this->response;
    }

    /**
     * Reading a contact
     *
     * @param string $id
     * @return Response
     */
    public function readAction($id)
    {
        $contact = Contacts::findFirst("id = " . $id . " AND deleted_on IS NULL");
        if(!$contact) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['input_error' => 'Contact not found']);
        } else {
            $this->response->setJsonContent($contact->toArray());
        }

        return $this->response;
    }

    /**
     * Deleting a contact
     *
     * @param string $id
     * @return Response
     */
    public function deleteAction($id)
    {
        $contact = Contacts::findFirst("id = " . $id . " AND deleted_on IS NULL");

        if (!$contact) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['input_error' =>'Contact not found']);
        } else {
            $contact->delete();
            $this->response->setJsonContent($contact->toArray());
        }

        return $this->response;
    }

    /**
     * @param $data
     * @param $limit
     * @param $page
     * @return \stdClass
     */
    private function getPaginator($data, $limit, $page)
    {
        return (new Paginator(
            [
                'data'  => $data,
                'limit' => $limit,
                'page'  => $page,
            ]
        ))->getPaginate();
    }

}
