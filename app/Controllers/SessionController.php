<?php
/**
 * Created by PhpStorm.
 * User: zhanarok
 * Date: 2019-08-12
 * Time: 14:50
 */

namespace App\Controllers;

use App\Models\Users;
use Phalcon\Mvc\Controller;


class SessionController extends Controller
{
    public function oauthRedirectAction()
    {
        $provider = $this->request->getQuery('provider');

        if(!isset($provider)) {
            $this->response->setJsonContent(['invalid_provider' => 'Provider is not set']);
        }
        $authUrl = $this->getDI()->getShared('oauth')->getAuthorizationUrl($provider);
        $this->response->redirect($authUrl, true);
        $this->response->send();
        return false;
    }

    public function oauthAction()
    {
        $provider = $this->request->getQuery('provider');
        $code = $this->request->get('code');

        if (empty($code)) {
            $this->response->setStatusCode(400);
            $this->response->setJsonContent(['invalid_code' =>'The auth provider information is invalid']);
        } else {
            $ownerDetails = $this->getDI()->getShared('oauth')->checkOauth($provider, $code);
            $email = $ownerDetails->getEmail();
            if(isset($email)) {
                $user = Users::findFirstByEmail($email);
                if (!$user) {
                    $user = new Users();
                    $user->email = $email;
                    $user->save();
                }
                $token = $this->generateJWT($user);
                $this->response->setJsonContent(['token' => $token]);
            } else {
                $this->response->setStatusCode(500, 'Internal Server Error')
                    ->setJsonContent([
                        'code' => 500,
                        'message' => 'Something went wrong. Please try again later'
                    ]);
            }
        }

        return $this->response;
    }

    public function generateJWT($user)
    {
        $payload = [
            'sub'   => $user->id,
            'email' => $user->email,
            'iat'   => time()
        ];
        $token = $this->auth->make($payload);

        return $token;
    }
}