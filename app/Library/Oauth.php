<?php
/**
 * Created by PhpStorm.
 * User: zhanarok
 * Date: 2019-08-12
 * Time: 15:03
 */

namespace App\Library;

use Phalcon\Di\Injectable;


class Oauth extends Injectable
{
    public function checkOauth($providerName, $code)
    {
        switch ($providerName) {
            case 'google':
                $provider = $this->getDI()->getShared('oauthProviderGoogle');
                break;
            default:
                throw new \Exception('Invalid oauth provider');
                break;
        }

        $token = $provider->getAccessToken('authorization_code', ['code' => $code]);

        return $provider->getResourceOwner($token);
    }

    public function getAuthorizationUrl($providerName)
    {
        switch ($providerName) {
            case 'google':
                $authUrl = $this->getDI()->getShared('oauthProviderGoogle')->getAuthorizationUrl();
                break;
            default:
                throw new \Exception('Invalid oauth provider');
                break;
        }

        return $authUrl;
    }
}