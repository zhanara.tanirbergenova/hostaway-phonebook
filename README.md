# Hostaway phonebook application

RESTful API to store, retrieve, delete, update phone book items.

## Built With

* Phalcon 3.4 (php 7.2)
* [OAuth 2 Google](https://github.com/thephpleague/oauth2-google) for authentication to access CRUD endpoints
* [Auth middleware](https://github.com/dmkit/phalcon-jwt-auth) JWT middleware to handle authentication
* [Guzzle](http://docs.guzzlephp.org/en/stable/) for making HTTP requests
* Mysql 8.0 & PhpMyAdmin
* Redis
* Nginx  

## What is done

* CRUD + search parts of the name
* OAuth2 authentication
* Pagination
* Errors logging
* Caching of external API calls

## How to test
* Go to /session/oauth/?provider=google, login with your gmail and get jwt token
* For each request add Header "Authorization: Bearer {jwt token}"  