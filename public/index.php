<?php


/**
 * useful constants
 */
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
date_default_timezone_set('Asia/Almaty');

try {
    $config = require APP_PATH . '/config/config.php';

    // Autoload
    require APP_PATH . '/config/loader.php';

    /**
     * Initializing DI container
     * @var \Phalcon\DI\FactoryDefault $di
     */
    $di = require APP_PATH . '/config/di.php';

    /**
     * Initializing application
     */
    $app = new \Phalcon\Mvc\Micro($di);

    include APP_PATH . '/config/routes.php';

    $auth = new Dmkit\Phalcon\Auth\Middleware\Micro($app, (array)$config->auth);

    // Processing request
   try {
       $app->handle($_SERVER['REQUEST_URI']);
   } catch (\Exception $e) {
       $logMessage = "Error in request {$_SERVER["REQUEST_URI"]}:\n" .
                     "Error Code: {$e->getCode()}\n" .
                     "Error Message: {$e->getMessage()}\n";

       $di['logger']->warning($logMessage);
       $app->response->setStatusCode(400, 'Bad Request')
           ->setJsonContent([
               'code' => 400,
               'message' => 'Bad request'
           ])->send();
   }
} catch (\Exception $e) {
    if (isset($di['logger'])) {
        // Exception logging
        $logMessage = "Error in request:\n" .
                      "Error Code: {$e->getCode()}\n" .
                      "Error Message: {$e->getMessage()}\n";

        $di['logger']->log(Phalcon\Logger::CRITICAL, $logMessage);
    } else {
        // If Exception was thrown before Phalcon Logger initialization
        error_log(
            "Exception {$e->getMessage()}, trace: {$e->getTraceAsString()}"
        );
    }

    $app->response->setStatusCode(500, 'Internal Server Error')
        ->setJsonContent([
            'code' => 500,
            'message' => 'Some error occurred on the server.'
        ])->send();
}
